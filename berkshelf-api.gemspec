#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "berkshelf-api"
  s.version = "2.2.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Jamie Winsor", "Andrew Garson"]
  s.date = "2016-08-03"
  s.description = "Berkshelf dependency API server"
  s.email = ["jamie@vialstudios.com", "agarson@riotgames"]
  s.executables = ["berks-api"]
  s.files = ["CHANGELOG.md", "LICENSE", "README.md", "bin/berks-api", "lib/berkshelf-api.rb", "lib/berkshelf/api.rb", "lib/berkshelf/api/application.rb", "lib/berkshelf/api/cache_builder.rb", "lib/berkshelf/api/cache_builder/worker.rb", "lib/berkshelf/api/cache_builder/worker/chef_server.rb", "lib/berkshelf/api/cache_builder/worker/file_store.rb", "lib/berkshelf/api/cache_builder/worker/github.rb", "lib/berkshelf/api/cache_builder/worker/supermarket.rb", "lib/berkshelf/api/cache_manager.rb", "lib/berkshelf/api/config.rb", "lib/berkshelf/api/core_ext.rb", "lib/berkshelf/api/core_ext/pathname.rb", "lib/berkshelf/api/cucumber.rb", "lib/berkshelf/api/dependency_cache.rb", "lib/berkshelf/api/endpoint.rb", "lib/berkshelf/api/endpoint/v1.rb", "lib/berkshelf/api/errors.rb", "lib/berkshelf/api/generic_server.rb", "lib/berkshelf/api/logging.rb", "lib/berkshelf/api/mixin.rb", "lib/berkshelf/api/mixin/services.rb", "lib/berkshelf/api/rack_app.rb", "lib/berkshelf/api/remote_cookbook.rb", "lib/berkshelf/api/rest_gateway.rb", "lib/berkshelf/api/rspec.rb", "lib/berkshelf/api/rspec/server.rb", "lib/berkshelf/api/site_connector.rb", "lib/berkshelf/api/site_connector/supermarket.rb", "lib/berkshelf/api/srv_ctl.rb", "lib/berkshelf/api/version.rb", "spec/fixtures/cookbooks/example_cookbook/metadata.rb", "spec/spec_helper.rb", "spec/support/actor_mocking.rb", "spec/support/chef_server.rb", "spec/support/human_reaable.rb", "spec/unit/berkshelf/api/application_spec.rb", "spec/unit/berkshelf/api/cache_builder/worker/chef_server_spec.rb", "spec/unit/berkshelf/api/cache_builder/worker/file_store_spec.rb", "spec/unit/berkshelf/api/cache_builder/worker/github_spec.rb", "spec/unit/berkshelf/api/cache_builder/worker/supermarket_spec.rb", "spec/unit/berkshelf/api/cache_builder/worker_spec.rb", "spec/unit/berkshelf/api/cache_builder_spec.rb", "spec/unit/berkshelf/api/cache_manager_spec.rb", "spec/unit/berkshelf/api/config_spec.rb", "spec/unit/berkshelf/api/dependency_cache_spec.rb", "spec/unit/berkshelf/api/endpoint/v1_spec.rb", "spec/unit/berkshelf/api/logging_spec.rb", "spec/unit/berkshelf/api/mixin/services_spec.rb", "spec/unit/berkshelf/api/rack_app_spec.rb", "spec/unit/berkshelf/api/rest_gateway_spec.rb", "spec/unit/berkshelf/api/site_connector/supermarket_spec.rb", "spec/unit/berkshelf/api/srv_ctl_spec.rb"]
  s.homepage = "https://github.com/berkshelf/berkshelf-api"
  s.licenses = ["Apache 2.0"]
  s.require_paths = ["lib"]
  s.required_ruby_version = Gem::Requirement.new(">= 2.1.0")
  s.rubygems_version = "1.8.23"
  s.summary = "A server which indexes cookbooks from various sources and hosts it over a REST API"
  s.test_files = ["spec/fixtures/cookbooks/example_cookbook/metadata.rb", "spec/spec_helper.rb", "spec/support/actor_mocking.rb", "spec/support/chef_server.rb", "spec/support/human_reaable.rb", "spec/unit/berkshelf/api/application_spec.rb", "spec/unit/berkshelf/api/cache_builder/worker/chef_server_spec.rb", "spec/unit/berkshelf/api/cache_builder/worker/file_store_spec.rb", "spec/unit/berkshelf/api/cache_builder/worker/github_spec.rb", "spec/unit/berkshelf/api/cache_builder/worker/supermarket_spec.rb", "spec/unit/berkshelf/api/cache_builder/worker_spec.rb", "spec/unit/berkshelf/api/cache_builder_spec.rb", "spec/unit/berkshelf/api/cache_manager_spec.rb", "spec/unit/berkshelf/api/config_spec.rb", "spec/unit/berkshelf/api/dependency_cache_spec.rb", "spec/unit/berkshelf/api/endpoint/v1_spec.rb", "spec/unit/berkshelf/api/logging_spec.rb", "spec/unit/berkshelf/api/mixin/services_spec.rb", "spec/unit/berkshelf/api/rack_app_spec.rb", "spec/unit/berkshelf/api/rest_gateway_spec.rb", "spec/unit/berkshelf/api/site_connector/supermarket_spec.rb", "spec/unit/berkshelf/api/srv_ctl_spec.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activesupport>, ["~> 4.0"])
      s.add_runtime_dependency(%q<archive>, ["= 0.0.6"])
      s.add_runtime_dependency(%q<buff-config>, ["~> 1.0"])
      s.add_runtime_dependency(%q<celluloid>, ["~> 0.16"])
      s.add_runtime_dependency(%q<celluloid-io>, ["~> 0.16"])
      s.add_runtime_dependency(%q<grape>, ["~> 0.14"])
      s.add_runtime_dependency(%q<grape-msgpack>, ["~> 0.1"])
      s.add_runtime_dependency(%q<hashie>, ["< 4.0.0", ">= 2.0.4"])
      s.add_runtime_dependency(%q<octokit>, ["< 5.0.0", ">= 3.0.0"])
      s.add_runtime_dependency(%q<rack>, ["~> 1.0"])
      s.add_runtime_dependency(%q<reel>, ["~> 0.6.0"])
      s.add_runtime_dependency(%q<ridley>, ["~> 4.4"])
      s.add_runtime_dependency(%q<semverse>, ["~> 1.0"])
      s.add_runtime_dependency(%q<varia_model>, ["~> 0.4"])
    else
      s.add_dependency(%q<activesupport>, ["~> 4.0"])
      s.add_dependency(%q<archive>, ["= 0.0.6"])
      s.add_dependency(%q<buff-config>, ["~> 1.0"])
      s.add_dependency(%q<celluloid>, ["~> 0.16"])
      s.add_dependency(%q<celluloid-io>, ["~> 0.16"])
      s.add_dependency(%q<grape>, ["~> 0.14"])
      s.add_dependency(%q<grape-msgpack>, ["~> 0.1"])
      s.add_dependency(%q<hashie>, ["< 4.0.0", ">= 2.0.4"])
      s.add_dependency(%q<octokit>, ["< 5.0.0", ">= 3.0.0"])
      s.add_dependency(%q<rack>, ["~> 1.0"])
      s.add_dependency(%q<reel>, ["~> 0.6.0"])
      s.add_dependency(%q<ridley>, ["~> 4.4"])
      s.add_dependency(%q<semverse>, ["~> 1.0"])
      s.add_dependency(%q<varia_model>, ["~> 0.4"])
    end
  else
    s.add_dependency(%q<activesupport>, ["~> 4.0"])
    s.add_dependency(%q<archive>, ["= 0.0.6"])
    s.add_dependency(%q<buff-config>, ["~> 1.0"])
    s.add_dependency(%q<celluloid>, ["~> 0.16"])
    s.add_dependency(%q<celluloid-io>, ["~> 0.16"])
    s.add_dependency(%q<grape>, ["~> 0.14"])
    s.add_dependency(%q<grape-msgpack>, ["~> 0.1"])
    s.add_dependency(%q<hashie>, ["< 4.0.0", ">= 2.0.4"])
    s.add_dependency(%q<octokit>, ["< 5.0.0", ">= 3.0.0"])
    s.add_dependency(%q<rack>, ["~> 1.0"])
    s.add_dependency(%q<reel>, ["~> 0.6.0"])
    s.add_dependency(%q<ridley>, ["~> 4.4"])
    s.add_dependency(%q<semverse>, ["~> 1.0"])
    s.add_dependency(%q<varia_model>, ["~> 0.4"])
  end
end
